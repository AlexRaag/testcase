//
// Created by Alex on 26.02.2018.
//

#pragma once

#include <SFML/System.hpp>
#include <SFML/Graphics/View.hpp>

#include <memory>
#include <SFML/Graphics.hpp>


class Camera {
public:
        using Ptr = std::unique_ptr<Camera>;
public:
        Camera();
        ~Camera();

        Camera(const Camera&)= delete;
        Camera& operator=(const Camera&)= delete;

        void setRenderTarget(sf::RenderTarget*);


        //Manuplulate stuff
        void setSpeed(float);
        void setSize(sf::Vector2f);
        void setCenter(sf::Vector2f);


        float getSpeed()const;
        sf::Vector2f getSize();
        sf::Vector2f getCenter();
        void setZoom(float);

        //returns View reference , for setting to Window
        sf::View& getCamera();

        //Coord Transformations//
        sf::Vector2i mapCoordToPixel(sf::Vector2f point);
        sf::Vector2f mapPixelToCoord(sf::Vector2i point);

        void reScale(float factor);

        void update(sf::Time deltaTime);
protected:
        sf::RenderTarget* target;

        sf::View cameraView;
        float speed;

        sf::Vector2f Size;
        sf::Vector2f Center;

        sf::FloatRect viewPort;
        sf::FloatRect viewRect;

        float zoomFactor;
};
