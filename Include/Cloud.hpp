//
// Created by Alex on 26.02.2018.
//

#pragma once

#include <list>
#include <memory>

#include <SFML/Graphics.hpp>

//Dont use enum class,because want cast enum to int
enum cloudDirection{
    left=-1,right=1
};

class Cloud: public sf::Drawable {
public:
    using Ptr = std::unique_ptr<Cloud>;
public:
    Cloud(const short& _direction,float winSize);
    ~Cloud();

    bool isLife() const;
    void setLife(bool life);

    void setTexture(const sf::Texture&);
    void reScale(float winSize);

    void update(sf::Time);
private:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    void setup();
private:
    sf::Vector2f position;
    sf::Sprite sprite;

    float speed;
    bool life;

    short direction;
    float winSizeX;
};


