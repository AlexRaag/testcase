//
// Created by Alex on 26.02.2018.
//

#pragma once

#include <memory>
#include <SFML/Graphics.hpp>

class SceneDelegate;


class AppGui{
public:
    using Ptr = std::unique_ptr<AppGui>;
public:
    AppGui(sf::RenderWindow&);
    ~AppGui();

    //del func//
    AppGui(const AppGui&)= delete;
    AppGui& operator=(const AppGui&)= delete;


    //Shound Show Gui or not
    bool isShowGui() const;
    void setShowGui(bool showGui);

    void reScale(float factor);
    void setScene(std::shared_ptr<SceneDelegate>);

    //Allow separate Gui && Scene for Mouse Pressing
    bool isMouseOver()const;


    void handleEvent(const sf::Event& event);
    void update(sf::Time);
    void render();

private:
    void setupWidgets();

    //Gui callbacks//
    void onClearButton();
    void onGenClouds();
    void onColorSlider(short it);
    void onIntensitySlider();

private:
    sf::RenderWindow& window;
    //pointer to scene,allows to manipulate to scene
    std::shared_ptr<SceneDelegate> scene;

    sf::Vector2f size;
    sf::Vector2f lastpos;

    float my_color[4];
    float colorSlider[4];
    float intensitySlider;

    bool showGui;
};
