//
// Created by Alex on 27.02.2018.
//

#pragma once

#include <GL/gl.h>
#include <memory>
#include <vector>
#include <list>
#include <Particle.hpp>

const GLint FIREWORK_PARTICLES = 70;

//Firework - particle system,in this way
//should create custom transformations &&
//make fireworks drawable
class Firework{
public:
    using Ptr = std::unique_ptr<Firework>;
public:
    Firework(sf::Vector2f mPos);
    ~Firework();

    //del Firework
    bool shouldRemove()const;
    void setShouldRemove(bool flag);



    //if not explode,will move at first;
    void update(sf::Time);
    void render();
private:

    void move(sf::Time);
    bool Init(sf::Vector2f mPos);
    void Explode();

private:
    std::list<Particle> particles;
    std::list<sf::Vertex> vertices;

    //color on colorShader//
    sf::Shader* colorShader;

    bool _shouldRemove;
    bool explode;

    sf::Time lifetime;
    sf::Vector2f emitter;


private:
    std::vector<sf::Vector2f> positions;
    std::vector<sf::Vector2f> speeds;

    GLfloat x[FIREWORK_PARTICLES];
    GLfloat y[FIREWORK_PARTICLES];
    GLfloat xSpeed[FIREWORK_PARTICLES];
    GLfloat ySpeed[FIREWORK_PARTICLES];

    GLfloat red;
    GLfloat blue;
    GLfloat green;
    GLfloat alpha;

    GLfloat particleSize;

    const GLfloat GRAVITY        = 0.05f;
    const GLfloat baselineYSpeed = -4.0f;
    const GLfloat maxYSpeed      = -4.0f;
};
