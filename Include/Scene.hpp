//
// Created by Alex on 26.02.2018.
//

#pragma once

#include <memory>
#include <list>
#include <SFML/Graphics.hpp>

class Firework;
class ParticleSystem;
class Cloud;


// Objective-C / Swift style
// use delegate for working gui && scene
// because gui not need all scene possibility
class SceneDelegate{
public:
    SceneDelegate();
    virtual ~SceneDelegate()=0;

    virtual void GenClouds()=0;
    virtual void ClearFireworks()=0;
    //TODO vec 4//
    virtual void setFireworkColor()=0;
    virtual void setIntensity(float _sliderVal)=0;
};


//Allow to draw Scene in sf::RenderWindow pipeline
// ,dont giving refence to it,because give refence
// steals a lot of resoures && slows gpu render
class Scene:public sf::Drawable,public SceneDelegate{
public:
    using Ptr = std::shared_ptr<Scene>();
public:
    Scene(sf::Vector2f);
    ~Scene();

    Scene(const Scene&)= delete;
    Scene& operator=(const Scene&)= delete;

    void addSystem(std::shared_ptr<ParticleSystem>);
    void addFirework(std::unique_ptr<Firework> firework);

    void update(sf::Time);


    inline size_t getSize()const{
        return systems.size();
    }
    std::shared_ptr<ParticleSystem> getSystemById(short id)const;

    //OPENGL Render because need more options for cool drawing//

public:
    void reScale(float factor);

    void GenClouds();
    void ClearFireworks();
    //TODO vec 4//
    void setFireworkColor();
    void setIntensity(float _sliderVal);

private:
    virtual void draw(sf::RenderTarget&
            ,sf::RenderStates)const override ;
    void fireworkRender() const;

    void setup();
    void setupOpenGL();
    void generateClouds();
private:
    sf::Vector2f winSize;

    //contais all live systems//
    std::list<std::shared_ptr<ParticleSystem>> systems;

    std::list<std::unique_ptr<Firework>> fireworks;
    //std::vector<std::unique_ptr<Firework>> fw;

    std::list<std::unique_ptr<Cloud>> clouds;
    float scaleFactor;

    sf::Sprite skySprite;
    sf::Texture skyTexture;
    sf::Texture cloudTexture;

    sf::Time cloudTimer;
};

inline void RandInit(){
    srand(time(NULL));
}