//
// Created by Alex on 26.02.2018.
//

#pragma once

#include <memory>
#include <SFML/Graphics.hpp>

#include <list>



class Particle:public sf::Drawable
        ,sf::Transformable{
public:
    Particle();
    ~Particle();

//    Particle(const Particle&)= delete;
//    Particle& operator=(const Particle&)= delete;


    void update(sf::Time);
private:
    virtual void draw(sf::RenderTarget& target
            ,sf::RenderStates)const override ;

private:
    std::list<sf::Vertex> vertices;

    sf::Time elapsedTime;
    bool life;
};


//REMOVE

struct ParticleSystemData{
    ParticleSystemData():size()
            ,intensity(1.f)
            {}

    sf::Vector2f size;
    float intensity;

    float color[4];
};



//Will be Firework
class ParticleSystem:public sf::Drawable{
public:
    using Ptr = std::shared_ptr<ParticleSystem>;
public:
    ParticleSystem(short particlesCount
            ,sf::Vector2f mPos,short _id);
    ~ParticleSystem();


    //del func

    ParticleSystem(const ParticleSystem&)= delete;
    ParticleSystem& operator=(const ParticleSystem&)= delete;


    bool shouldRemove()const;
    void setShouldRemove(bool flag);

    void update(sf::Time);

    ParticleSystemData* getData();
    short getID() const;
private:
    virtual  void draw(sf::RenderTarget &target
            , sf::RenderStates states) const override;

    void setup(short );
    void resetParticle(std::size_t index);

private:
    bool _shouldRemove;
    short id;
    std::vector<Particle> particles;
    std::list<sf::Vertex> vertices;

    sf::Time lifetime;
    sf::Vector2f emitter;

    ParticleSystemData* data;
};

