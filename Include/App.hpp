//
// Created by Alex on 26.02.2018.
//

#pragma once


#include <SFML/Graphics.hpp>
#include <Util/Util.hpp>

#include "Camera.hpp"
#include "AppGui.hpp"

class Scene;
class App{
public:
    App(const sf::ContextSettings& settings);
    ~App();

    //del func//
    App(const App&)= delete;
    App& operator=(const App&)= delete;


    bool LoadIni();
    void Run();
private:

    void handleEvents();
    void update();
    void render();

    void setup();
    void setupSystem(sf::Vector2f);

    void processInput(const  sf::Event& event);
private:
    sf::RenderWindow window;
    //Scene class
    std::shared_ptr<Scene> scene;

    sf::Time TimePerFrame;

    Camera camera;
    AppGui gui;


    sf::Text appDescription;
    sf::Font font;

    sf::Sprite skySprite;
    sf::Texture skyTexture;
    sf::Texture cloudTexture;

    //last Window Size for rescaling
    sf::Vector2f lastSize;
};
