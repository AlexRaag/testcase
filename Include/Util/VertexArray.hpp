//
// Created by Alex on 26.02.2018.
//

#pragma once

#include <SFML/Graphics.hpp>

namespace Case{
    class VertexArray{
    public:
        VertexArray();
        ~VertexArray();


        VertexArray(const VertexArray&)= delete;
        VertexArray& operator=(const VertexArray&)= delete;

        bool isEmpty()const;


    private:
        std::vector<sf::Vertex> m_vertices;      ///< Vertices contained in the array
        sf::PrimitiveType       m_primitiveType;
    };
}
