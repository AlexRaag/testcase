//
// Created by Alex on 26.02.2018.
//


#include <cmath>
#include <Particle.hpp>




Particle::Particle() {

}

Particle::~Particle() {

}

void Particle::update(sf::Time deltaTime) {

}

void Particle::draw(sf::RenderTarget &target, sf::RenderStates) const {

}





ParticleSystem::ParticleSystem(short particlesCount
        ,sf::Vector2f mPos,short _id)
        :_shouldRemove(false)
        ,emitter(mPos)
        ,id(_id)
        ,data(nullptr)
         {

    data = new ParticleSystemData();
    setup(particlesCount);
}

ParticleSystem::~ParticleSystem() {
    delete data;
}


bool ParticleSystem::shouldRemove() const {
    return _shouldRemove;
}



void ParticleSystem::update(sf::Time deltaTime) {

//    for(auto it:particles){
//
//    }

//    vertices.remove_if([this](sf::Vertex vertex) {
//
//    });

}


void ParticleSystem::setShouldRemove(bool flag) {
    _shouldRemove=flag;
}


void ParticleSystem::draw(sf::RenderTarget &target
        , sf::RenderStates states) const {

    for(auto& it:particles){
        target.draw(it,states);
    }

   // target.draw();
}



void ParticleSystem::setup(short particlesCount) {
    //
    for(short it=0;it<particlesCount;++it){
        particles.emplace_back(Particle());
        vertices.emplace_back(sf::Vertex());
    }


}

void ParticleSystem::resetParticle(std::size_t index) {

        // give a random velocity and lifetime to the particle
        float angle = (std::rand() % 360) * 3.14f / 180.f;
        float speed = (std::rand() % 50) + 50.f;


//        particles[index].velocity = sf::Vector2f(std::cos(angle) * speed
//                , std::sin(angle) * speed);
//        particles[index].lifetime = sf::milliseconds((std::rand() % 2000) + 1000);
//
//        // reset the position of the corresponding vertex
//        vertices[index].position = emitter;

}


short ParticleSystem::getID() const {
    return id;
}




ParticleSystemData* ParticleSystem::getData()  {
     if(data != nullptr)
        return data;
}




