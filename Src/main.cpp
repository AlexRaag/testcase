#include <App.hpp>

int main() {

    sf::ContextSettings settings;
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 4;
    settings.majorVersion = 2;
    settings.minorVersion = 1;

    App app(settings);

    if(!app.LoadIni()){
        print("Error to load");
        return 1;
    }
    try {
        app.Run();
    }
    catch(const std::exception& ex){
        print(ex.what());
    }

    return 0;
}