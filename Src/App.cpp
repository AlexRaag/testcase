//
// Created by Alex on 26.02.2018.
//

#include <cmath>
#include <SFML/System/Time.hpp>


#include <App.hpp>
#include <Scene.hpp>
#include <Particle.hpp>
#include <Firework.hpp>

//TODO window flags//


App::App(const sf::ContextSettings& settings)
        :window(sf::VideoMode(1920,1080),"TestCase",sf::Style::Default,settings)
        ,gui(window)
        ,scene(nullptr)
        ,appDescription()
        ,font()
        ,TimePerFrame(sf::seconds(1.f/60.f)) {
        window.setFramerateLimit(60);
   setup();
}

App::~App() {

}

bool App::LoadIni() {
    return  true;
}



void App::Run() {
    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;

    while (window.isOpen()) {
        sf::Time delta  = clock.restart();
        timeSinceLastUpdate += delta;

        while(timeSinceLastUpdate > TimePerFrame) {
            timeSinceLastUpdate -= TimePerFrame;

            handleEvents();

        }
        update();
        render();
    }
}

void App::handleEvents() {
    sf::Event event;
    while(window.pollEvent(event)){
        if(event.type == sf::Event::Closed)
            window.close();

        //if App Was Resized need reScale Scene,gui,Camera
        if(event.type == sf::Event::Resized){
            //TODO calc scale factor

            auto _factor = sf::Vector2f(float(event.size.width/lastSize.x)
                    ,float (event.size.height/lastSize.y));

//            scene->reScale(_factor);
//            gui.reScale(_factor);
//            camera.reScale(_factor);


            //Redo on Camera
            sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
            window.setView(sf::View(visibleArea));
        }

        processInput(event);
        gui.handleEvent(event);
    }
}

void App::update() {
        gui.update(sf::seconds(1.f/60.f));
        camera.update(sf::seconds(1.f/60.f));
        scene->update(sf::seconds(1.f/60.f));
}

void App::render() {
    window.clear(sf::Color::White);

    window.draw(*scene);
    window.draw(appDescription);


    if(gui.isShowGui())
        gui.render();

    window.display();
}

void App::setup() {



    scene = std::make_shared<Scene>(sf::Vector2f(window.getSize()));
    gui.setScene(scene);

    sf::Vector2f size = sf::Vector2f(window.getSize());
    lastSize =  size;

    font.loadFromFile("Resources/arial.ttf");
    appDescription.setFont(font);

    appDescription.setString("Please read Readme.txt");
    appDescription.setCharacterSize(30);

    auto bounds = appDescription.getLocalBounds();
    appDescription.setOrigin(std::floor(bounds.left + bounds.width / 2.f)
            , std::floor(bounds.top + bounds.height / 2.f));

    appDescription.setPosition(size.x/2,size.y*0.9);

    RandInit();
}

void App::processInput(const sf::Event &event) {

    //Check if Key H was Pressed
    if(event.type == sf::Event::KeyPressed
       && event.key.code == sf::Keyboard::H) {
        if(!gui.isShowGui())
            gui.setShowGui(true);
        else
            gui.setShowGui(false);
    }


    //Create Firework everyWhere
    if(event.type == sf::Event::MouseButtonPressed
       &&event.mouseButton.button == sf::Mouse::Left
       && !gui.isMouseOver()) {
        setupSystem(sf::Vector2f(event.mouseButton.x
                ,event.mouseButton.y));
    }

}

void App::setupSystem(sf::Vector2f mPos) {
    ParticleSystem::Ptr system = std::make_shared<ParticleSystem>(1000
            ,mPos,scene->getSize());

    scene->addSystem(system);

    std::unique_ptr<Firework> firework = std::make_unique<Firework>(mPos);
    scene->addFirework(std::move(firework));
}
