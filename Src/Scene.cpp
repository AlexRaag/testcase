//
// Created by Alex on 26.02.2018.
//

#include <cmath>
#include <algorithm>

#include <SFML/OpenGL.hpp>

#include <Scene.hpp>
#include <Util/Util.hpp>
#include <Particle.hpp>
#include <Cloud.hpp>
#include <Firework.hpp>

SceneDelegate::SceneDelegate() {

}

SceneDelegate::~SceneDelegate(){

}




Scene::Scene(sf::Vector2f _size)
        :scaleFactor(1.f)
        ,skySprite()
        ,skyTexture()
        ,cloudTexture()
        ,winSize(_size)
        ,cloudTimer(sf::seconds(5.f))
{
    setup();
}

Scene::~Scene() {

}



void Scene::addSystem(std::shared_ptr<ParticleSystem> system) {
    if(system != nullptr)
        systems.emplace_back(system);
}



void Scene::update(sf::Time delta) {
    for(auto& it:systems)
        it->update(delta);

   for(auto& cloud:clouds)
       cloud->update(delta);

    for (auto& it:fireworks)
        it->update(delta);

    // removes all systems in was Set remove //
    systems.remove_if([this](ParticleSystem::Ptr& system){
        return system->shouldRemove();
    });

    //removes clouds out of defalut scene view
    clouds.remove_if([this](Cloud::Ptr& cloud){
        return !cloud->isLife();
    });

    fireworks.remove_if([this](Firework::Ptr& firework){
        return firework->shouldRemove();
    });
    //auto genclouds//
    cloudTimer -= delta;
    if(cloudTimer <= sf::Time::Zero){
        generateClouds();
        cloudTimer = sf::seconds(5.f);
    }
}


void Scene::draw(sf::RenderTarget &target, sf::RenderStates states) const {

    target.pushGLStates();
    target.draw(skySprite);
    for(auto& cloud:clouds)
        target.draw(*cloud,states);
    target.popGLStates();


    fireworkRender();

}

void Scene::reScale(float newfactor) {
//    for(auto &it:clouds)
//        it->reScale();
}

void Scene::setup() {
    setupOpenGL();
    skyTexture.loadFromFile("Resources/skyTexture.png");
    cloudTexture.loadFromFile("Resources/cloudTexture.png");

    skySprite.setTexture(skyTexture);


    sf::Vector2f sizeTex=sf::Vector2f(skyTexture.getSize());
    print(sizeTex.x,sizeTex.y);

    sf::Vector2f _scaleFactor = sf::Vector2f(float(winSize.x/sizeTex.x)
            ,float(winSize.y/sizeTex.y));

    skySprite.setScale(_scaleFactor);
    skySprite.setPosition(0,0);

    for(short it=0;it<3;++it)
        generateClouds();
}

void Scene::generateClouds() {
    //TODO reWrite
    for(short it=0;it<3;++it) {
        //rand direciton//
        short r  = rand()%2;
        if(r == 0)
            r=-1;

        Cloud::Ptr cloud = std::make_unique<Cloud>(r
                ,winSize.x);

        cloud->setTexture(cloudTexture);
        clouds.emplace_back(std::move(cloud));
    }
}

void Scene::GenClouds() {
    generateClouds();
}

void Scene::ClearFireworks() {
    fireworks.clear();
}

ParticleSystem::Ptr Scene::getSystemById(short id) const {
    auto iter =  find_if(systems.begin(),systems.end()
            ,[&](ParticleSystem::Ptr ptr){
        return ptr->getID() == id;
    });
    if(iter != systems.end()) {
        auto found = (*iter);
        return found;
    }
}



void Scene::setFireworkColor() {

}


void Scene::setIntensity(float _sliderVal) {

}


void Scene::fireworkRender() const{
    glClear(GL_ACCUM_BUFFER_BIT);

    glTranslatef(0.375, 0.375, 0);
    // Draw our fireworks
    for(auto& firework:fireworks)
        firework->render();
    glPopMatrix();


    glAccum(GL_ACCUM, 0.99f);
}

void Scene::setupOpenGL() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Enable blending (we need this to be able to use an alpha component)
    glDisable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearAccum(0.0f, 0.0f, 0.0f, 1.0f);

    glEnable(GL_POINT_SMOOTH); // Smooth the points so that they're circular and not square
}

void Scene::addFirework(std::unique_ptr<Firework> firework) {
    if(firework != nullptr)
        fireworks.emplace_back(std::move(firework));
}



