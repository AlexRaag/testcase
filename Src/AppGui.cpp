//
// Created by Alex on 26.02.2018.
//

#include <imgui/imgui.h>
#include <imgui/imgui-SFML.h>

#include <Util/Util.hpp>
#include <AppGui.hpp>
#include <Scene.hpp>



AppGui::AppGui(sf::RenderWindow& _window
        )
        :window(_window)
        ,showGui(true)
        ,scene(nullptr)
        ,intensitySlider(0.0f)
        ,colorSlider()
{
    ImGui::SFML::Init(window);

    for(short it=0;it<4;++it){
        my_color[it]=0.5;
        colorSlider[it]=0.f;
    }
}

AppGui::~AppGui() {
    ImGui::SFML::Shutdown();
}

void AppGui::handleEvent(const sf::Event &event) {
    ImGui::SFML::ProcessEvent(event);
}

void AppGui::update(sf::Time deltaTime) {
    //TODO check flags//
    ImGui::SFML::Update(window, deltaTime);
    setupWidgets();
}

void AppGui::render() {
    ImGui::SFML::Render(window);
}

void AppGui::setupWidgets() {

    ImGui::Begin("ToolBar Window");

    //Allow to change color
    if(ImGui::ColorEdit4("Color",my_color)){
        onColorSlider(1);
    }

    //TODO manupulator for Stuff
    ImGui::SliderFloat("Intensity", &intensitySlider ,
                       0.f, 10.0f, "%.4f", 1.0f);


    if(ImGui::Button("GenClouds"))
        onGenClouds();
    ImGui::SameLine(0,10);
    if(ImGui::Button("ClearParticles"))
        onClearButton();


    if(lastpos.x != ImGui::GetWindowPos().x
       && lastpos.y != ImGui::GetWindowPos().y)
        lastpos = ImGui::GetWindowPos();

    if(size.x != ImGui::GetWindowSize().x
       && size.y != ImGui::GetWindowSize().y)
        size = ImGui::GetWindowSize();


    ImGui::End();
}


bool AppGui::isShowGui() const {
    return showGui;
}

void AppGui::setShowGui(bool showGui) {
    AppGui::showGui = showGui;
}

void AppGui::setScene(std::shared_ptr<SceneDelegate> _Scene) {
    if(_Scene != nullptr)
        scene = _Scene;
    else
        print("Scene == nullprt");
}

void AppGui::reScale(float factor) {
    //TODO Gui reScale
}


//DONE//
void AppGui::onClearButton() {
    scene->ClearFireworks();
}

void AppGui::onGenClouds() {
    scene->GenClouds();
}

void AppGui::onIntensitySlider() {
    scene->setIntensity(intensitySlider);
}
//DONE//


void AppGui::onColorSlider(short it) {
    ImVec4 color;

    color.x = my_color[0];
    color.y = my_color[1];
    color.z = my_color[2];
    color.w = my_color[3];


    //scene->setFireworkColor(color);
}


bool AppGui::isMouseOver() const {
    auto mPos = sf::Mouse::getPosition(window);

    sf::FloatRect rect(lastpos,size);
    return rect.contains(mPos.x,mPos.y);
}


