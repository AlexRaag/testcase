//
// Created by Alex on 27.02.2018.
//

#include <Firework.hpp>





Firework::Firework(sf::Vector2f mPos):
        _shouldRemove(false)
        ,explode(true) {
    Init(mPos);
}

Firework::~Firework() {

}


void Firework::update(sf::Time delta) {
    if(explode)
        Explode();
    else
        move(delta);
}

//TODO rewrite//

bool Firework::Init(sf::Vector2f mPos) {

    // Pick an initial x location and random x/y speeds for each particle making up the firework
    // Note: Each particle in the firework must have the exact same values for the firework to rise as a single point!
    float xLoc      = ((float)rand() / (float)RAND_MAX) * 1920.0f;
    float xSpeedVal = -2 + ((float)rand() / (float)RAND_MAX) * 4.0f;
    float ySpeedVal = baselineYSpeed + ((float)rand() / (float)RAND_MAX) * maxYSpeed;


    // Set initial x/y location and speeds for each particle in the firework
    for (int loop = 0; loop < FIREWORK_PARTICLES; ++loop)
    {
        sf::Vector2f curPos,curSpeed;

        curPos.x = mPos.x;
        curPos.y = mPos.y;
        curSpeed.x = xSpeedVal;
        curSpeed.y = ySpeedVal;

        positions.emplace_back(curPos);
        speeds.emplace_back(curSpeed);
    }

    // Assign a random colour and full alpha (i.e. particle is completely opaque)
    red   = ((float)rand() / (float)RAND_MAX);
    green = ((float)rand() / (float)RAND_MAX);
    blue  = ((float)rand() / (float)RAND_MAX);
    alpha = 1.0f;


    // Size of the particle (as thrown to glPointSize) - range is 1.0f to 4.0f
    particleSize = 1.0f + ((float)rand() / (float)RAND_MAX) * 3.0f;

    for (int loop2 = 0; loop2 < FIREWORK_PARTICLES; loop2++)
    {
        // Set a random x and y speed beteen -4 and + 4
        speeds[loop2].x = -4 + (rand() / (float)RAND_MAX) * 8;
        speeds[loop2].y = -4 + (rand() / (float)RAND_MAX) * 8;
    }

    return true;
}

void Firework::Explode() {
    for (int loop = 0; loop < FIREWORK_PARTICLES; loop++)
    {
        // Dampen the horizontal speed by 1% per frame
        speeds[loop].x *= 0.99;

        // Move the particle
        positions[loop].x += speeds[loop].x;
        positions[loop].y += speeds[loop].y;

        // Apply gravity to the particle's speed
        speeds[loop].y += Firework::GRAVITY;
    }

    // Fade out the particles (alpha is stored per firework, not per particle)
    if (alpha > 0.0f)
        alpha -= 0.01f;
    else // Once the alpha hits zero, then reset the firework{
       _shouldRemove=true;

}


void Firework::move(sf::Time) {
    if(1>2)
        explode=true;
}


bool Firework::shouldRemove() const {
    return _shouldRemove;
}

void Firework::setShouldRemove(bool flag) {
    _shouldRemove=flag;
}

void Firework::render() {
    for (int loop = 0; loop < FIREWORK_PARTICLES; loop++) {
        glPointSize(particleSize);

        glBegin(GL_POINTS);

        glColor4f(red, green, blue, alpha);
        // Draw the point
        glVertex2f(positions[loop].x, positions[loop].y);

        glEnd();
    }
}




