//
// Created by Alex on 26.02.2018.
//

#include <cassert>
#include <Camera.hpp>

Camera::Camera() {

}

Camera::~Camera() {

}

void Camera::update(sf::Time deltaTime) {

}

void Camera::setRenderTarget(sf::RenderTarget *t) {
    assert(t != nullptr);
    target = t;
}



void Camera::setSize(sf::Vector2f _size) {
    Size = _size;
    cameraView.setSize(Size);
}

void Camera::setCenter(sf::Vector2f _center) {
    Center = _center;
    cameraView.setCenter(_center);
}

void Camera::setSpeed(float s) {
    speed = s;
}

sf::Vector2f Camera::getSize() {
    return  Size;
}

sf::View& Camera::getCamera() {
    return cameraView;
}

sf::Vector2f Camera::getCenter() {
    return Center;
}

float Camera::getSpeed() const {
    return speed;
}

sf::Vector2i Camera::mapCoordToPixel(sf::Vector2f point) {
    return target->mapCoordsToPixel(point);
}

sf::Vector2f Camera::mapPixelToCoord(sf::Vector2i point) {
    return target->mapPixelToCoords(point);
}

void Camera::setZoom(float factor) {
    zoomFactor = factor;
    //TODO
    // cameraView.zoom(1.f);
    cameraView.zoom(zoomFactor);
}


void Camera::reScale(float factor) {

}
