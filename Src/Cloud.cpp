//
// Created by Alex on 26.02.2018.
//

#include <Cloud.hpp>

Cloud::Cloud(const short& _direction,float _winSizeX)
        :sprite()
        ,speed(0.05f)
        ,life(true)
        ,direction(_direction)
        ,winSizeX(_winSizeX) {

    setup();
}

Cloud::~Cloud() {

}

void Cloud::update(sf::Time deltaTime) {
    //TODO directon

    position.x += deltaTime.asMilliseconds()*speed*direction;
    sprite.setPosition(position);

    //Should deleta clouds if out of default view
    if(direction == cloudDirection::left){
        if (position.x < 0.f)
            life=false;
    } else {
        if (position.x > winSizeX)
            life=false;
    }
}


void Cloud::draw(sf::RenderTarget &target,sf::RenderStates states) const {
    target.draw(sprite);
}

bool Cloud::isLife() const {
    return life;
}

void Cloud::setLife(bool life) {
    Cloud::life = life;
}

void Cloud::setTexture(const sf::Texture &tex) {
    sprite.setTexture(tex);

    sprite.setScale(0.2,0.2);

    auto bounds = sprite.getLocalBounds();
    sprite.setOrigin(bounds.width/2,bounds.height/2);
}

void Cloud::reScale(float winSize) {
    winSizeX = winSize;
    //TODO rescale
}

void Cloud::setup() {
    position.x = 50 + rand()%int(winSizeX/1.3);
    position.y = 50 + rand()%300;
}

